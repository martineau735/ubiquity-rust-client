# TxOutputs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | Option<**i32**> | Number of total items | [optional]
**data** | Option<[**Vec<crate::models::TxOutputsData>**](tx_outputs_data.md)> |  | [optional]
**meta** | Option<[**crate::models::Meta**](meta.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


