# BlockIdentifier

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | Option<**i64**> | Block number | [optional]
**id** | Option<**String**> | Block hash | [optional]
**parent_id** | Option<**String**> | Parent block hash | [optional]
**date** | Option<**i64**> | Block date in Unix timestamp format | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


