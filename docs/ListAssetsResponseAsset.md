# ListAssetsResponseAsset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**contract_address** | Option<**String**> |  | [optional]
**wallets** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


