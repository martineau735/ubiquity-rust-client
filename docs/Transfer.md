# Transfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **String** | Sender address | 
**to** | **String** | Receiver address | 
**currency** | [**crate::models::Currency**](currency.md) |  | 
**value** | **String** | Integer string in smallest unit (Satoshis) | 
**fee** | Option<[**crate::models::Fee**](fee.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


