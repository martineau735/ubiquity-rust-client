# Asset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_id** | Option<**i64**> |  | [optional]
**image_url** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**contract** | Option<[**crate::models::Contract**](Contract.md)> |  | [optional]
**wallets** | Option<[**Vec<crate::models::AssetWallet>**](AssetWallet.md)> |  | [optional]
**attributes** | Option<[**Vec<crate::models::AssetTrait>**](AssetTrait.md)> |  | [optional]
**mint_date** | Option<**i64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


