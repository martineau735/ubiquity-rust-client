# GetAssetResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset** | Option<[**crate::models::GetAssetResponseAsset**](GetAssetResponse_Asset.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


