# TxCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **String** | The source UTXO or account ID for the originating funds | 
**to** | [**Vec<crate::models::TxDestination>**](tx_destination.md) | A list of recipients | 
**index** | **f32** | The UTXO index or the account Nonce | 
**fee** | Option<**String**> | The fee you are willing to pay (required only for Ethereum) for the transaction | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


