# TxMinify

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | Unique transaction identifier | [optional]
**date** | Option<**i64**> | Unix timestamp | [optional]
**block_id** | Option<**String**> | ID of block. | [optional]
**block_number** | Option<**i64**> | Height of block, | [optional]
**confirmations** | Option<**i64**> | Total transaction confirmations | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


