# ProtocolsOverviewProtocols

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handle** | Option<**String**> |  | [optional]
**network** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


