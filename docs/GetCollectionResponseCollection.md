# GetCollectionResponseCollection

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**logo** | Option<**String**> |  | [optional]
**banner** | Option<**String**> |  | [optional]
**verified** | Option<**bool**> |  | [optional]
**contracts** | Option<[**Vec<crate::models::Contract>**](Contract.md)> |  | [optional]
**meta** | Option<[**serde_json::Value**](.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


