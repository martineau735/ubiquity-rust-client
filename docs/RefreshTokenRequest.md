# RefreshTokenRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contract_address** | Option<**String**> | Mapped to body `contract_addresses` | [optional]
**token_id** | Option<**String**> | Mapped to body `token_id` | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


