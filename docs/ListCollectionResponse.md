# ListCollectionResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | Option<[**Vec<crate::models::Collection>**](Collection.md)> |  | [optional]
**meta** | Option<[**crate::models::Meta**](Meta.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


