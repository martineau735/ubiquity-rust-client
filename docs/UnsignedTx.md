# UnsignedTx

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> | The transaction ID | [optional]
**unsigned_tx** | **String** | The transaction data needed to sign | 
**meta** | Option<[**serde_json::Value**](.md)> | Any extra information relevant regarding the created transaction | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


