# GetEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**contract_address** | Option<**String**> |  | [optional]
**token_id** | Option<**String**> |  | [optional]
**event_type** | Option<**String**> |  | [optional]
**timestamp** | Option<**i64**> |  | [optional]
**from** | Option<**String**> |  | [optional]
**to** | Option<**String**> |  | [optional]
**quantity** | Option<**i64**> |  | [optional]
**transaction** | Option<[**serde_json::Value**](.md)> |  | [optional]
**payments** | Option<[**Vec<crate::models::Payment>**](Payment.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


