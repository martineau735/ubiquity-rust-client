# Supply

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maximum** | Option<**String**> | Maximum supply | [optional]
**total** | Option<**String**> | Total supply at block height, excluding burnt coins | [optional]
**total_created** | Option<**String**> | Total coins created historically up until this block | [optional]
**total_burnt** | Option<**String**> | Total coins burnt historically up until this block | [optional]
**created** | Option<**String**> | Coins created at this block | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


