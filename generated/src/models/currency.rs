/*
 * Universal REST API
 *
 * Universal API provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple protocols/cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Currently supported protocols:  * algorand   * mainnet * bitcoin   * mainnet/testnet * bitcoincash   * mainnet/testnet * dogecoin   * mainnet/testnet * ethereum   * mainnet/goerli * litecoin   * mainnet/testnet * near   * mainnet/testnet * oasis   * mainnet * optimism   * mainnet * polkadot   * mainnet/westend * polygon   * mainnet * solana   * mainnet/testnet * stellar   * mainnet/testnet * tezos   * mainnet * xrp   * mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 3.0.0
 * Contact: support@blockdaemon.com
 * Generated by: https://openapi-generator.tech
 */



#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Currency {
    #[serde(rename="native")]
    NativeCurrency {
        /// Asset path of transferred currency
        #[serde(rename = "asset_path")]
        asset_path: String,
        /// Currency symbol
        #[serde(rename = "symbol", skip_serializing_if = "Option::is_none")]
        symbol: Option<String>,
        /// Name of currency
        #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
        name: Option<String>,
        /// Decimal places right to the comma
        #[serde(rename = "decimals", skip_serializing_if = "Option::is_none")]
        decimals: Option<i32>,
    },
    #[serde(rename="smart_token")]
    SmartTokenCurrency {
        /// Asset path of transferred currency
        #[serde(rename = "asset_path")]
        asset_path: String,
        /// Currency symbol
        #[serde(rename = "symbol", skip_serializing_if = "Option::is_none")]
        symbol: Option<String>,
        /// Name of currency
        #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
        name: Option<String>,
        /// Decimal places right to the comma
        #[serde(rename = "decimals", skip_serializing_if = "Option::is_none")]
        decimals: Option<i32>,
        #[serde(rename = "detail", skip_serializing_if = "Option::is_none")]
        detail: Option<Box<crate::models::SmartToken>>,
    },
    #[serde(rename="token")]
    TokenCurrency {
        /// Asset path of transferred currency
        #[serde(rename = "asset_path")]
        asset_path: String,
        /// Currency symbol
        #[serde(rename = "symbol", skip_serializing_if = "Option::is_none")]
        symbol: Option<String>,
        /// Name of currency
        #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
        name: Option<String>,
        /// Decimal places right to the comma
        #[serde(rename = "decimals", skip_serializing_if = "Option::is_none")]
        decimals: Option<i32>,
        #[serde(rename = "detail", skip_serializing_if = "Option::is_none")]
        detail: Option<Box<crate::models::Token>>,
    },
}




